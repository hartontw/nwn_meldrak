# Meldrak

This repository contains unpacked files of Meldrak module for Neverwinter Nights: Enhanced Edition.

To Pack and Unpack the module [Jakob Knutsen](https://github.com/jakkn)'s [nwn-devbase](https://github.com/jakkn/nwn-devbase) is used.